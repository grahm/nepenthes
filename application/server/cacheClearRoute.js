// Import Express and Middleware
import express from 'express'
import apicache from 'apicache'

// Setup Router
let router = express.Router()

// Clear cache based on target
router.get('/cache/clear/:target?', function(req, res) {
  console.log('Cache Clear Target: ' + req.params.target)
  res.json(apicache.clear(req.params.target))
})

export default router
