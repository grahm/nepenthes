'use strict'

// Import Express and Middleware
import express from 'express'
import path from 'path'
import favicon from 'serve-favicon'
import logger from 'morgan'
// import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import compression from 'compression'
import apicache from 'apicache'

// Import Application routes
import contentfulEntryApiRoute from './contentfulEntryApiRoute'
import cacheClearRoute from './cacheClearRoute'
import preactApplicationRoute from './preactApplicationRoute'

import faviconFile from '../resources/icons/favicon.png'

// Require Polyfills
require('babel-polyfill')

// Setup Express Server
let app = express()

// Setup Middleware
let cache = apicache.middleware

app.use(compression())
app.use(favicon(path.join(__dirname, '../../', faviconFile)))
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))
app.use('/public', express.static(path.join(__dirname, '../../public')))
app.use('/service-worker-registrar.js', express.static(path.join(__dirname, '../../application/service-worker-registrar.js')))
app.use('/service-worker.js', express.static(path.join(__dirname, '../../application/service-worker.js')))

// Setup Routes
app.get('/api/entry/:entry', cache('7 days'), contentfulEntryApiRoute)
app.get('/cache/clear:target?', cacheClearRoute)
app.get('/*', cache('7 days'), preactApplicationRoute)

export default app
