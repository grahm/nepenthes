// Import Express and Application DOM Renderer
import express from 'express'
import Document from '../containers/Document'

// Setup Router
let router = express.Router()

// Render the Preact Application
router.get('/*', function(req, res) {
  // Set cache group to default
  req.apicacheGroup = 'default'

  // Render Preact Page
  const documentDom = new Document()
  documentDom.handleRender(req, res)
})

export default router
