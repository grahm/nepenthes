// Import
import {h, Component} from 'preact'
import Helmet from 'preact-helmet'
import manifest from '../data/manifest.json'
import icon from '../resources/icons/favicon.png'

class Application extends Component {
  // Render Preact component
  render() {
    return (
      <div id="Application">
        {this.props.children}

        <Helmet
          title={manifest.name}
          htmlAttributes={{
            amp: undefined,
            itemscope: true,
            lang: manifest.lang,
            prefix: 'og: http://ogp.me/ns#'
          }}
          meta={[
            {charSet: `utf-8`},
            {httpEquiv: `x-ua-compatible`, content: `ie=edge`},
            {name: `mobile-web-app-capable`, content: `yes`},
            {name: `apple-mobile-web-app-capable`, content: `yes`},
            {name: `application-name`, content: manifest.name},
            {name: `apple-mobile-web-app-title`, content: manifest.name},
            {name: `theme-color`, content: manifest.theme_color},
            {name: `msapplication-navbutton-color`, content: manifest.theme_color},
            {name: `apple-mobile-web-app-status-bar-style`, content: `black-translucent`},
            {name: `msapplication-starturl`, content: `/`},
            {name: `viewport`, content: `width=device-width, initial-scale=1`}
          ]}
          link={[
            {
              rel: `preload`,
              as: `image`,
              href: icon
            },
            {
              rel: `manifest`,
              href: `/public/manifest.json`
            },
            {
              rel: `icon`,
              type: `image/png`,
              sizes: `128x128`,
              href: icon
            },
            {
              rel: `apple-touch-icon`,
              type: `image/png`,
              sizes: `128x128`,
              href: icon
            }
          ]}
        />
      </div>
    )
  }
}

export default Application
