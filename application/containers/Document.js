// Prevent async loading of pages
import pages from '../pages'

// Import node modules
import {h} from 'preact'
import render from 'preact-render-to-string'
import {ServerStyleSheet} from 'styled-components'
import {Provider, IsomorphicFetch} from 'jsx-isomorphic-fetch'
import Helmet from 'preact-helmet'

// Import Application Files
import RouteManager from '../classes/RouteManager'
import Router from '../containers/Router'
import Application from '../containers/Application'

// Import application data
import manifest from '../data/manifest.json'
import stats from '../../public/stats.json'

export default class Document {
  constructor() {
    this.route = null
    this.chunkName = null
    this.RoutedLayoutComponent = null
    this.routeManager = new RouteManager()
    this.sheet = new ServerStyleSheet()
  }

  handleRender(req, res) {
    // Resolve route and get routing data
    this.route = this.routeManager.resolveRoute(req.url)
    this.chunkName = this.route.handler.chunkName

    let isomorphicFetch = new IsomorphicFetch()
    isomorphicFetch.isServer = true

    let application = (
      <Provider isomorphicFetch={isomorphicFetch}>
        <Application>
          <Router routeParams={this.route.params} DefaultComponent={pages[this.chunkName]} />
        </Application>
      </Provider>
    )

    render(application)
    isomorphicFetch
      .resolve()
      .then(() => {
        let appMarkup = render(this.sheet.collectStyles(application))
        const css = this.sheet.getStyleTags()
        const head = Helmet.rewind()

        res.send(
          `<!DOCTYPE html>
          <html>
            <head>
              ${head.title.toString()}
              ${head.meta.toString()}
              ${head.link.toString()}

              <script>window._stats_ = ${JSON.stringify(stats)}</script>
              <script>window._isomorphicFetch_store_ = ${isomorphicFetch.store}</script>
              <script defer src="${stats.publicPath + stats.assetsByChunkName.manifest}"></script>
              <script defer src="${stats.publicPath + stats.assetsByChunkName.vendor}"></script>
              <script defer src="${stats.publicPath + stats.assetsByChunkName[this.chunkName]}"></script>
              <script defer src="/service-worker-registrar.js"></script>

              ${css}
            </head>
            <body>
              <div id="mount">${appMarkup}</div>
            </body>
          </html>`
        )
      })
      .catch(error => {
        console.error(error)
        res.send('503 Server Error')
      })
  }
}
