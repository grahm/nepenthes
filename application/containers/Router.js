// Import
import {h, Component} from 'preact'
import RouteManager from '../classes/RouteManager'
import {isClient} from '../data/settings'

export default class Router extends Component {
  constructor(props) {
    super(props)

    this.state = {
      RoutedComponent: this.props.DefaultComponent,
      routeParams: this.props.routeParams ? this.props.routeParams : {}
    }
  }

  // Setup navigation event callbacks
  watchLocation() {
    if (isClient) {
      document.addEventListener('historyRouteUpdate', this.updateRoute.bind(this), false)
      window.onpopstate = this.updateRoute.bind(this)
    }
  }

  // Update route displayed to the user
  updateRoute() {
    let route = this.routeManager.resolveRoute(window.location.pathname)
    route.handler.callback(route.params, states => {
      this.setState(states)
    })
  }

  // Run browser setup functions when component mounts
  componentDidMount() {
    if (isClient) {
      this.routeManager = new RouteManager()
      this.watchLocation()
      this.updateRoute()
    }
  }

  // Render Preact component
  render() {
    return (
      <div>
        <this.state.RoutedComponent {...this.state.routeParams} />
      </div>
    )
  }
}
