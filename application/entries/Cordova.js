// Prevent async loading of pages
import pages from '../pages'

// Import node modules
import {h, render} from 'preact'
import {Provider, IsomorphicFetch} from 'jsx-isomorphic-fetch'

// Import Application Files
import RouteManager from '../classes/RouteManager'
import Router from '../containers/Router'
import Application from '../containers/Application'

document.addEventListener(
  'deviceready',
  () => {
    history.pushState({}, 'Index', '/')

    // Create preact promise manager and set initial data
    let isomorphicFetch = new IsomorphicFetch()
    isomorphicFetch.store = {}

    // Get initial routing
    let routeManager = new RouteManager()
    let route = routeManager.resolveRoute(window.location.pathname)

    render(
      <Provider isomorphicFetch={isomorphicFetch}>
        <Application>
          <Router routeParams={route.params} DefaultComponent={pages['cordovaEntry']} />
        </Application>
      </Provider>,
      document.querySelector('#mount'),
      document.querySelector('#Application')
    )
  },
  false
)
