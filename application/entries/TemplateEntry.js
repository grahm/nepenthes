// Import node modules
import {h, render} from 'preact'
import {Provider, IsomorphicFetch} from 'jsx-isomorphic-fetch'

// Import Application Files
import RouteManager from '../classes/RouteManager'
import Router from '../containers/Router'
import Application from '../containers/Application'
import TemplateLayout from '../pages/TemplateLayout'

// Lazyload scripts
import lazyloadjs from '../helpers/lazyloadjs'
lazyloadjs()

// Create preact promise manager and set initial data
let isomorphicFetch = new IsomorphicFetch()
isomorphicFetch.store = window._isomorphicFetch_store_

// Get initial routing
let routeManager = new RouteManager()
let route = routeManager.resolveRoute(window.location.pathname)

render(
  <Provider isomorphicFetch={isomorphicFetch}>
    <Application>
      <Router routeParams={route.params} DefaultComponent={TemplateLayout} />
    </Application>
  </Provider>,
  document.querySelector('#mount'),
  document.querySelector('#Application')
)
