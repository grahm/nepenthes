import Error404Layout from './Error404Layout'
import TemplateLayout from './TemplateLayout'

const pages = {
  Error404Layout: Error404Layout,
  TemplateLayout: TemplateLayout,
  cordovaEntry: TemplateLayout
}

export default pages
