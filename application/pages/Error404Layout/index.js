// Import node modules
import {h, Component} from 'preact'
import Helmet from 'preact-helmet'
import PropTypes from 'prop-types'

import Link from '../../components/Link'
import {origin} from '../../data/settings'

// Setup Layout Preact Component
class Error404Layout extends Component {
  render() {
    return (
      <div>
        <Helmet title={`Test Puppers ${this.state.title}`} />
        Hello World: cordova working?
        <Link href="/">Test Link</Link>
      </div>
    )
  }
}

Error404Layout.contextTypes = {
  isomorphicFetch: PropTypes.func.isRequired
}

export default Error404Layout
