// Import node modules
import {h, Component} from 'preact'
import PropTypes from 'prop-types'

import Link from '../../components/Link'
import {H1Default} from '../../components/H1'

export default class TemplateLayout extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentWillMount() {
    const initialData = this.context.isomorphicFetch.initial('https://jsonplaceholder.typicode.com/posts/1')
    this.state.title = initialData ? initialData.title : null

    // Trigger fetching of data
    this.context.isomorphicFetch
      .fetch(['https://jsonplaceholder.typicode.com/posts/1'], response => {
        return response.json()
      })
      .then(data => {
        this.setState({
          title: data.title
        })
      })
  }

  componentWillUnmount() {
    this.context.isomorphicFetch.clear('https://jsonplaceholder.typicode.com/posts/1')
  }

  render() {
    return (
      <div>
        <H1Default>
          {this.state.title ? this.state.title : ''}
        </H1Default>
        <Link href="/test">Test Link</Link>
      </div>
    )
  }
}

TemplateLayout.contextTypes = {
  isomorphicFetch: PropTypes.func.isRequired
}
