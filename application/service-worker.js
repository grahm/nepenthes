var CACHE = 'cache-and-update'

self.addEventListener('install', function(evt) {
  console.log(CACHE + ': The service worker is being installed.')
  evt.waitUntil(precache())
})

self.addEventListener('fetch', function(evt) {
  console.info({
    type: String(evt.request.url).indexOf('public') !== -1 ? 'instant' : 'network',
    cache: CACHE,
    req: evt.request
  })

  if (String(evt.request.url).indexOf('public') !== -1) {
    evt.respondWith(fromCache(evt.request))
    evt.waitUntil(update(evt.request))
  } else {
    evt.respondWith(
      fromNetwork(evt.request, 3500).catch(function() {
        return fromCache(evt.request)
      })
    )
  }
})

function precache() {
  return caches.open(CACHE).then(function(cache) {
    return cache.addAll(['./'])
  })
}

function fromNetwork(request, timeout) {
  // eslint-disable-next-line
  return new Promise(function(fulfill, reject) {
    var timeoutId = setTimeout(reject, timeout)
    fetch(request).then(function(response) {
      clearTimeout(timeoutId)
      fulfill(response)
      update(request)
    }, reject)
  })
}

function fromCache(request) {
  return caches.open(CACHE).then(function(cache) {
    return cache.match(request).then(function(matching) {
      // eslint-disable-next-line
      return matching || Promise.reject('no-match')
    })
  })
}

function update(request) {
  return caches.open(CACHE).then(function(cache) {
    return fetch(request).then(function(response) {
      return cache.put(request, response)
    })
  })
}
