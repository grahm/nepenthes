import styled from 'styled-components'

const H1Default = styled.h1`
  font-size: 1.5em;
  text-align: center;
  color: white;
  background-color: green;
  line-height: 2em;
`

export {H1Default}
