// Import node modules
import {h, Component} from 'preact'
import PropTypes from 'prop-types'

import {isClient} from '../data/settings'

// Setup Link Preact Component
class Link extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isActive: isClient && window.location.pathname + window.location.hash === this.props.href
    }
  }

  // On click function
  goTo(e) {
    if (this.props.href.charAt(0) === '/') {
      e.preventDefault()
      history.pushState({}, this.props.hasOwnProperty('title') ? this.props.title : String(this.props.children), this.props.href)

      if (window.location.hash !== '') {
        let hash = window.location.hash.split('!')[1]
        let anchor = document.getElementById(hash)
        window.scroll(0, anchor.offsetTop - 70)
      } else {
        window.scroll(0, 0)
      }

      // Trigger route change event
      const event = new Event('historyRouteUpdate')
      document.dispatchEvent(event)
      this.updateState()

      if (this.props.callback) this.props.callback()
    }
  }

  componentDidMount() {
    document.addEventListener('historyRouteUpdate', this.updateState.bind(this), false)
  }

  updateState() {
    this.setState({
      isActive: isClient && window.location.pathname + window.location.hash === this.props.href
    })
  }

  // Render Preact component
  render() {
    var attributes = {}
    if (this.state.isActive) {
      attributes.active = true
    }

    return (
      <a {...this.props} {...attributes} href={this.props.href} onClick={this.goTo.bind(this)}>
        {this.props.children}
      </a>
    )
  }
}

export default Link
