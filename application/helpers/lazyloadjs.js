const lazyLoadScript = require('lazyload-script')

export default function lazyloadjs() {
  let lazyjs = window._stats_.lazyload
  for (var i = 0; i < lazyjs.length; i++) {
    lazyLoadScript(window._stats_.publicPath + lazyjs[i])
  }
}
