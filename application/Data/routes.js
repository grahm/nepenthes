import {isClient} from './settings'

const routes = [
  // TemplateLayout Route
  {
    path: '/index',
    handler: {
      chunkName: 'TemplateLayout',
      callback: function(params, routeHandler) {
        if (isClient) {
          require.ensure([], require => {
            let TemplateLayout = require('../pages/TemplateLayout')
            routeHandler({
              RoutedComponent: TemplateLayout.default,
              routeParams: params
            })
          })
        }
      }
    }
  },

  // 404 RouteManager
  {
    path: '/404',
    handler: {
      chunkName: 'Error404Layout',
      callback: function(params, routeHandler) {
        if (isClient) {
          require.ensure([], require => {
            let Error404Layout = require('../pages/Error404Layout')
            routeHandler({
              RoutedComponent: Error404Layout.default,
              routeParams: params
            })
          })
        }
      }
    }
  }
]

export default routes
