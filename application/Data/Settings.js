const isClient = typeof window !== 'undefined'

const hostname = isClient ? window.location.hostname : '127.0.0.1'
const port = isClient ? window.location.port : '3000'
const protocol = isClient ? window.location.protocol : 'http:'
const origin = `${protocol}//${hostname}${port ? ':' + port : ''}`

export {isClient, origin, hostname, port}
