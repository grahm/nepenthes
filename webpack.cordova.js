const webpack = require('webpack')
const path = require('path')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = function(env) {
  return {
    entry: {
      entry: path.join(__dirname, './application/entries/Cordova.js')
    },

    output: {
      path: path.join(__dirname, './cordova/www'),
      filename: '[name].bundle.js',
      publicPath: '/'
    },

    devtool: 'eval-source-map',

    resolve: {
      extensions: ['.js', '.jsx'],
      alias: {
        react: 'preact-compat',
        'react-dom': 'preact-compat'
      }
    },

    module: {
      loaders: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        },
        {
          test: /\.json$/,
          use: 'json-loader'
        },
        {
          test: /\.(png|jpg|gif)$/,
          use: [
            {
              loader: 'file-loader',
              options: {}
            }
          ]
        }
      ]
    },

    plugins: [
      new CleanWebpackPlugin(['./cordova/www/*']),

      new CopyWebpackPlugin([
        {
          from: 'application/resources/cordova/index.html',
          to: 'index.html'
        },
        {
          from: 'application/data/manifest.json',
          to: 'manifest.json'
        },
        {
          from: 'application/resources/icons',
          to: 'icons'
        }
      ])
    ]
  }
}
