const webpack = require('webpack')
const path = require('path')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = function() {
  return {
    target: 'node',

    entry: {
      server: path.join(__dirname, './application/www.js')
    },

    output: {
      path: path.join(__dirname, './bin'),
      filename: 'scripts/[name].js',
      publicPath: '/public/'
    },

    node: {
      console: true,
      global: false,
      process: false,
      Buffer: false,
      __filename: false,
      __dirname: false
    },

    resolve: {
      extensions: ['.js', '.jsx'],
      alias: {
        react: 'preact-compat',
        'react-dom': 'preact-compat'
      }
    },

    module: {
      loaders: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        },
        {
          test: /\.json$/,
          use: 'json-loader'
        },
        {
          test: /\.(png|jpg|gif)$/,
          use: [
            {
              loader: 'file-loader',
              options: {}
            }
          ]
        }
      ]
    },

    plugins: [new CleanWebpackPlugin(['./bin/*'])]
  }
}
