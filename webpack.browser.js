const webpack = require('webpack')
const path = require('path')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = function(env) {
  return {
    entry: {
      vendor: ['preact', 'styled-components'],
      Error404Layout: path.join(__dirname, './application/entries/Error404Entry.js'),
      TemplateLayout: path.join(__dirname, './application/entries/TemplateEntry.js')
    },

    output: {
      path: path.join(__dirname, './public'),
      filename: '[chunkhash].[name].bundle.js',
      publicPath: '/public/'
    },

    devtool: 'eval-source-map',

    resolve: {
      extensions: ['.js', '.jsx'],
      alias: {
        react: 'preact-compat',
        'react-dom': 'preact-compat'
      }
    },

    module: {
      loaders: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        },
        {
          test: /\.json$/,
          use: 'json-loader'
        },
        {
          test: /\.(png|jpg|gif)$/,
          use: [
            {
              loader: 'file-loader',
              options: {}
            }
          ]
        }
      ]
    },

    plugins: [
      new CleanWebpackPlugin(['./public/*']),

      new webpack.optimize.CommonsChunkPlugin({
        name: ['vendor', 'manifest']
      }),

      new CopyWebpackPlugin([
        {
          from: 'application/data/manifest.json',
          to: 'manifest.json'
        },
        {
          from: 'application/resources/icons',
          to: 'icons'
        }
      ]),

      function() {
        this.plugin('done', function(stats) {
          lazyload = []
          for (var i = 0; i < stats.toJson().chunks.length; i++) {
            let chunk = stats.toJson().chunks[i]
            for (var ii = 0; ii < chunk.files.length; ii++) {
              lazyload.push(chunk.files[ii])
            }
          }

          require('fs').writeFileSync(
            path.join(__dirname, 'public', 'stats.json'),
            JSON.stringify({
              publicPath: stats.toJson().publicPath,
              assetsByChunkName: stats.toJson().assetsByChunkName,
              lazyload: lazyload
            })
          )
        })
      },

      function() {
        this.plugin('done', function(stats) {
          require('fs').writeFileSync(path.join(__dirname, 'public', 'stats-full.json'), JSON.stringify(stats.toJson()))
        })
      }
    ]
  }
}
